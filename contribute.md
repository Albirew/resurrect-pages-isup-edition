# Contribute
I love pull requests from everyone. 
In fact, since I don't have much time for coding (neither much skill, I must admit), any pull request is very welcome!

If you find a bug in the source code, you can help me by submitting an issue to this repository. 
Even better if you can submit a Pull Request with a fix.